const Course = require("../model/Course");

// Create a new course 
module.exports.addCourse = async (user, reqBody) => {
	if(user.isAdmin){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return (`You have no access`);
	}
}

// Controller method for retrieving all the courses 
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course
module.exports.updateCourse = (user, reqParams, reqBody) => {
	// Validate if user is Admin
	if (user.isAdmin){
		//Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		prices : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}
		else{
				return true;
			}
		})
	}else{
		return (`You have no access`);
	}
}

// Archive a Course
module.exports.archivedCourse = async (reqParams, reqBody, user) => {
	if(user.isAdmin){
		return Course.findById(reqParams.courseId).then(result => {
			result.isActive = false;

			return result.save().then((course, error) => {
				if(error){
					return false;
				} else {
					return `The course has been archived`;
				}
			})
		});

	} else {
		return `You have no access to archive the course.`;
	}
}