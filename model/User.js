const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required: [true, "First name is required"]
	},
	lastName : {
		type: String,
		required: [true, "Last name is required"]
	},
	email : {
		type: String,
		required: [true, "An email is required"]
	},
	password : {
		type: String,
		required: [true, "A password is required"]
	},
	isAdmin : {
		type: Boolean,
		default: true
	},
	mobileNo : {
		type: String,
		required: [true, "A mobile number is required"]
	},
	enrollments: [{
		courseId: {
			type: String,
			required: [true, "CourseId is required"]
		},
		enrolledOn: {
			type: Date,
			default: new Date()
		}
	}
	],
	status : {
		type: String,
		default: "Enrolled",
		required: true
	}
});

module.exports = mongoose.model("User", userSchema);