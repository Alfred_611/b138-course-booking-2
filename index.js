const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user"); // (ADDED IN SECOND PHASE)
const courseRoutes = require("./routes/course"); // (ADDED when course was made)

const app = express();

// Connect to MongoDB
mongoose.connect("mongodb+srv://dbalfredmanalang:MttlylLvtpD7OACh@wdc028-course-booking.qczdh.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once('open', () => console.log('Now connected to MongoDBAtlas'))

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file (ADDED IN SECOND PHASE)
app.use("/users", userRoutes);

// Defines the "/courses" string to be included for all course routes defined in the "course" route
app.use("/courses", courseRoutes);


// Listening to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process. env.PORT || 4000 }`)
});