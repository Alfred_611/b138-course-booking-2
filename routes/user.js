const express = require("express");
const router = express.Router();
const userController = require("../controllers/user"); //Added in second phase nung naasikaso na controllers
const auth = require("../auth");
// Route for checking if the user's email already exists in the database (Added in 2nd phase)
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


// Route for authenticating a user (MADE AFTER DOING JWT)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for getting the details of a user
// auth.verify method acts as a middleware to ensure that the user is logged in before they can access specific app features
router.get("/details", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the retrieveUser controller method
	userController.retrieveUser({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	const userData = auth.decode(req.headers.authorization);
	userController.enroll(data, userData).then(resultFromController => res.send(resultFromController));
})

// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;